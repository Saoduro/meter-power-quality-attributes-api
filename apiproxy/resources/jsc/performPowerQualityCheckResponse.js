 var response = context.getVariable("soapResponse.body");
 if (typeof response != 'undefined') {
   var responseBody = JSON.parse(response);
   var responseCode = responseBody.statusCode;
   var deviceMacId = responseBody.meterReadResult.deviceMacID;
   var deviceName = responseBody.meterReadResult.deviceName;
   var deviceSubType = responseBody.meterReadResult.deviceSubType;
   var deviceCapability = responseBody.meterReadResult.deviceCapability;
   var version = responseBody.meterReadResult.version.toString();
   var timestamp = responseBody.meterReadResult.timestamp;
   var meterRead = responseBody.meterReadResult.meterReadList.meterRead;

   var responseObject = {};
   var meters = {};
   var finalObject = {};

   if (responseCode === 'Success') {
     context.setVariable('response.status.code', 200);
     var correlationId = context.getVariable("correlationId");
     
     // map correlationId
     if (responseBody.requestID != "NULL") {
       responseObject.correlationId = correlationId;
     } else {
       responseObject.correlationId = "";
     }

     // map meters
     if (responseBody.statusCode != "NULL") {

       var powerQualityAttributes = [];
       
       	if (request.deviceUtilId != "NULL")
		{
		    
		var deviceUtilId = context.getVariable("request.queryparam.deviceUtilId");
		   
		meters.deviceUtilId = deviceUtilId;
		}
		else
		{
		meters.deviceUtilId = "";
		}
       meters.deviceMACId = deviceMacId;
       meters.deviceName = deviceName;
       meters.deviceSubType = deviceSubType;
       meters.deviceCapability = deviceCapability;
       meters.version = version;

       for (var count = 0; count < meterRead.length; count++) {
         {
          var finalPowerQualAttrObject = {};
           finalPowerQualAttrObject.name = meterRead[count].name;
           if (typeof meterRead[count].Value != 'undefined') {
           finalPowerQualAttrObject.value = meterRead[count].Value.toString();
           } else {
               finalPowerQualAttrObject.value = "";
           }
           powerQualityAttributes.push(finalPowerQualAttrObject);
           meters.powerQualityAttributes = powerQualityAttributes;

         }
       }

     }

     responseObject.meters = meters;

   }

   context.setVariable("finalResponse", JSON.stringify(responseObject));

 }