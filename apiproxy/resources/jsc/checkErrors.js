var response = context.getVariable("soapResponse.errorBody");
if (typeof response != 'undefined') {
var responseBody = JSON.parse(response);
var errCode = responseBody.Code.Subcode.Value;
var errorGeneral = responseBody.Detail.generalErrorDescription;
var errorDetail = responseBody.Detail.detailErrorDescription;
var errorDetailAlt = responseBody.Reason.Text.TEXT;

errCode = errCode.toString();

if (errCode.includes('FailedAuthentication')){
 errCode = '1020A';
 context.setVariable("customErrorDetail", errorDetailAlt);
} else if (errorDetail != "NULL") {
 context.setVariable("customErrorDetail", errorDetail);
} else {
 context.setVariable("customErrorDetail", errorGeneral); 
}


var statusCodes = {
"1020A": "401",
"1021": "500",
"1022": "400",
"1023": "404",
"1024": "503",
"NULL": "500"
};

print('statusCodes:',statusCodes[errCode]);

context.setVariable("response.status.code", statusCodes[errCode]);
}